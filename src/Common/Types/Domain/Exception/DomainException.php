<?php

declare(strict_types=1);

namespace ShoppingCart\Common\Types\Domain\Exception;

use DomainException as SPLDomainException;

abstract class DomainException extends SPLDomainException
{
    public function __construct()
    {
        parent::__construct($this->errorMessage());
    }

    abstract protected function errorMessage(): string;
}
