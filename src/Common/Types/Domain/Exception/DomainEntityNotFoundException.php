<?php
declare(strict_types=1);

namespace ShoppingCart\Common\Types\Domain\Exception;

abstract class DomainEntityNotFoundException extends DomainException
{

}
