<?php
declare(strict_types=1);

namespace ShoppingCart\Common\Types\Domain\Exception;

use ShoppingCart\Common\Types\Domain\Uuid;

class EventNotFoundInTheAggregateRootUnPersistedRecordedEventsException extends DomainException
{
    /**
     * @var string
     */
    private $errorMessage;

    public function __construct(Uuid $aggregateRootId, string $eventClassNamespace)
    {
        $this->errorMessage = "Domain event with namespace: \\$eventClassNamespace\\ not found in the non persisted"
            . "recorded events collection inside aggregate root with id: {$aggregateRootId->value()}";

        parent::__construct();
    }


    protected function errorMessage(): string
    {
        return $this->errorMessage;
    }
}
