<?php
declare(strict_types=1);

namespace ShoppingCart\Common\Types\Application;

/**
 * Interface DtoResponse
 */
interface DtoResponse
{
    public function toArray() : array;
}