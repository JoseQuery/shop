<?php

declare(strict_types=1);

namespace ShoppingCart\Common\Types\Application\CommandBus;

interface CommandHandler
{
    public function __invoke(Command $command): void;
}
