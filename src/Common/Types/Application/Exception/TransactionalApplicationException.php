<?php
declare(strict_types=1);

namespace ShoppingCart\Common\Types\Application\Exception;

class TransactionalApplicationException extends ApplicationException
{
}

