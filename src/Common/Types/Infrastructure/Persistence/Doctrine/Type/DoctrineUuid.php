<?php

declare(strict_types=1);

namespace ShoppingCart\Common\Types\Infrastructure\Persistence\Doctrine\Type;

use ShoppingCart\Common\Types\Domain\Uuid;

class DoctrineUuid extends DoctrineId
{
    public function className(): string
    {
        return Uuid::class;
    }
}
