<?php

namespace ShoppingCart\Common\Types\Infrastructure\Exception;

class IpInvalidArgumentInfrastructureException extends InvalidArgumentInfrastructureException
{

    protected function getName(): string
    {
        return 'ip';
    }
}
