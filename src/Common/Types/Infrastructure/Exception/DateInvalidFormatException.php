<?php

namespace ShoppingCart\Common\Types\Infrastructure\Exception;

use Exception;

class DateInvalidFormatException extends Exception
{
    private function __construct($msg)
    {
        parent::__construct($msg);
    }

    public static function make(string $format, string $variable)
    {
        $msg = $variable.' error format- '.$format;
        return new self($msg);
    }
}