<?php
declare(strict_types=1);

namespace ShoppingCart\Common\Types\Infrastructure\Exception;

class ApiGatewayServiceUnavailableInfrastructureException extends ApiGatewayInfrastructureException
{

}
