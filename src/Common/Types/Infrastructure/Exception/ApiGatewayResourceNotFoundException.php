<?php
declare(strict_types=1);

namespace ShoppingCart\Common\Types\Infrastructure\Exception;

class ApiGatewayResourceNotFoundException extends ApiGatewayInfrastructureException
{

}