<?php

namespace ShoppingCart\Common\Types\Infrastructure\Exception;

class IntegerInvalidArgumentInfrastructureException extends InvalidArgumentInfrastructureException
{
    protected function getName(): string
    {
        return 'integer';
    }
}
