<?php

namespace ShoppingCart\Common\Types\Infrastructure\Exception;

class BooleanInvalidArgumentInfrastructureException extends InvalidArgumentInfrastructureException
{

    protected function getName(): string
    {
        return 'boolean';
    }
}
