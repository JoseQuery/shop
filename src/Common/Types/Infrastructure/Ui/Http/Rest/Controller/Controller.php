<?php
declare(strict_types=1);

namespace ShoppingCart\Common\Types\Infrastructure\Ui\Http\Rest\Controller;

use ShoppingCart\Common\Types\Domain\Uuid;
use ShoppingCart\Common\Types\Infrastructure\Ui\Http\Rest\Exception\BadFormatException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints as Assert;
use ShoppingCart\Common\Types\Application\CommandBus\Command;
use ShoppingCart\Common\Types\Application\DtoResponse;
use ShoppingCart\Common\Types\Application\QueryBus\Query;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class Controller extends AbstractController
{
    /**
     * @var MessageBusInterface
     */
    protected $commandBus;

    /**
     * @var MessageBusInterface
     */
    protected $queryBus;

    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * Controller constructor.
     * @param MessageBusInterface $commandBus
     * @param MessageBusInterface $queryBus
     */
    public function __construct(MessageBusInterface $commandBus, MessageBusInterface $queryBus)
    {
        $this->commandBus = $commandBus;
        $this->queryBus = $queryBus;
        $this->validator = Validation::createValidator();
    }

    /**
     * @param array|null $response
     * @return JsonResponse
     */
    public function buildResponseForOk(array $response = null): JsonResponse
    {
        return new JsonResponse(
            $response !== null ? json_encode($response) : '',
            JsonResponse::HTTP_OK,
            [],
            true
        );
    }

    public function dispatchQuery(Query $query) : DtoResponse
    {
        $envelope = $this->queryBus->dispatch($query);
        /** @var HandledStamp $handled */
        $handled = $envelope->last(HandledStamp::class);

        return $handled->getResult();
    }

    public function dispatchCommand(Command $command) : ?array
    {
        $envelope = $this->commandBus->dispatch($command);
        /** @var HandledStamp $handled */
        $handled = $envelope->last(HandledStamp::class);

        if ($handled->getResult() instanceof Uuid) {
            return ['resource_id' => $handled->getResult()->value()];
        }

        return null;
    }

    /**
     * @param Assert\Collection $constraintCollection
     * @param array $contract
     * @throws BadFormatException
     */
    public function validate(Assert\Collection $constraintCollection, array $contract)
    {
        $violations = $this->validator->validate($contract, $constraintCollection);
        if ($violations->count() > 0) {
            throw new BadFormatException($violations->get(0));
        }
    }
}
