<?php
declare(strict_types=1);

namespace ShoppingCart\Common\Types\Infrastructure\Ui\Http\Rest\Controller;

use JMS\Serializer\Serializer;
use ShoppingCart\Common\Types\Application\CommandBus;
use ShoppingCart\Common\Types\Application\QueryBus;
use ShoppingCart\Common\Types\Infrastructure\Ui\Http\Rest\DataTransformer\IdentifiableDtoResourceToRestResourceDataTransformer;
use ShoppingCart\Common\Types\Infrastructure\Ui\Http\Rest\DataTransformer\NonIdentifiableDtoResourceToRestResourceDataTransformer;
use Psr\Log\LoggerInterface;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class HealthCheckController extends Controller
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        CommandBus $commandBus,
        QueryBus $queryBus,
        IdentifiableDtoResourceToRestResourceDataTransformer $identifiableDtoResourceToApiRestResourceDataTransformer,
        NonIdentifiableDtoResourceToRestResourceDataTransformer $nonIdentifiableDtoResourceToApiRestResourceDataTransformer,
        Serializer $serializer,
        LoggerInterface $logger
    ) {
        parent::__construct(
            $commandBus,
            $queryBus,
            $identifiableDtoResourceToApiRestResourceDataTransformer,
            $nonIdentifiableDtoResourceToApiRestResourceDataTransformer,
            $serializer
        );

        $this->logger = $logger;
    }

    /**
     *
     * @Route("/ping", name="get_ping", methods={"GET"})
     *
     * @SWG\Response(
     *     response="200",
     *     description="Ok",
     * )
     *
     * @SWG\Tag(name="Healthcheck")
     *
     * @return JsonResponse
     */
    public function getPing(): JsonResponse
    {
        $this->logger->debug(
            'API is healthy'
        );
        return new JsonResponse();
    }

    /**
     * @inheritDoc
     */
    protected function namespaces(): array
    {
        return array('common', 'healthcheck');
    }
}
