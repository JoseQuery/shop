<?php

declare(strict_types=1);

namespace ShoppingCart\Common\Types\Infrastructure\Symfony\Bundle;

use ShoppingCart\Common\Types\Infrastructure\Symfony\DependencyInjection\CommonExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class CommonBundle extends Bundle
{
    protected function getContainerExtensionClass()
    {
        return CommonExtension::class;
    }
}
