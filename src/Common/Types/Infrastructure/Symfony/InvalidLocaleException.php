<?php
declare(strict_types=1);

namespace ShoppingCart\Common\Types\Infrastructure\Symfony;

use ShoppingCart\Common\Types\Domain\Exception\InvalidDomainFormatException;

class InvalidLocaleException extends InvalidDomainFormatException
{

    protected function getName(): string
    {
        return 'locale';
    }

}
