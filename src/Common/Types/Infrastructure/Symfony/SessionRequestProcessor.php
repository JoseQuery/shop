<?php

namespace ShoppingCart\Common\Types\Infrastructure\Symfony;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class SessionRequestProcessor
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var string
     */
    private $userId;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function __invoke(array $record)
    {
        if (is_null($this->tokenStorage->getToken())) {
            return $record;
        }
        if (!$this->userId)  {
            $this->userId = $this->tokenStorage->getToken()->getUser()->id()->value() ?: '????????';
        }
        $record['extra']['user_id'] = $this->userId;
        return $record;
    }
}
