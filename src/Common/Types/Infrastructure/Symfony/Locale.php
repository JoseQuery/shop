<?php
declare(strict_types=1);

namespace ShoppingCart\Common\Types\Infrastructure\Symfony;

use ShoppingCart\Common\Types\Domain\ValueObject;

class Locale extends ValueObject
{

    const ES = 'ES';

    /**
     * @var string
     */
    private $value;

    /**
     * Locale constructor.
     * @param string $locale
     */
    public function __construct(string $locale)
    {
        $this->guard($locale);
        $this->value = $locale;
    }

    private function guard(string $locale): void
    {
        $this->guardValue($locale);
    }

    private function guardValue(string $locale): void
    {
        if (!$locale === self::ES) {
            throw new InvalidLocaleException($locale);
        }
    }

    /**
     * @return string
     */
    public function value(): string
    {
        return $this->value;
    }

    /**
     * @param ValueObject|Locale $o
     *
     * @return bool
     */
    protected function equalValues(ValueObject $o): bool
    {
        return $this->value() === $o->value();
    }
}
