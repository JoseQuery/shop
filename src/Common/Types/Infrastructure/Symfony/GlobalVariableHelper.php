<?php
declare(strict_types=1);

namespace ShoppingCart\Common\Types\Infrastructure\Symfony;

class GlobalVariableHelper
{
    public static function setLocale(Locale $locale): void
    {
        $GLOBALS['locale'] = $locale;
    }

    public static function locale(): Locale
    {
        return $GLOBALS['locale'];
    }
}
