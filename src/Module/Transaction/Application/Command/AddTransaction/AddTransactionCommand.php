<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Transaction\Application\Command\AddTransaction;

use ShoppingCart\Common\Types\Application\CommandBus\Command;

/**
 * Class AddTransactionCommand
 * @package ShoppingCart\Module\Transaction\Application\Transaction\Command\AddProduct
 */
class AddTransactionCommand implements Command
{
    /**
     * @var string
     */
    private $cartId;

    /**
     * AddTransactionCommand constructor.
     * @param string $cartId
     */
    public function __construct(string $cartId)
    {
        $this->cartId = $cartId;
    }

    /**
     * @return string
     */
    public function cartId(): string
    {
        return $this->cartId;
    }
}
