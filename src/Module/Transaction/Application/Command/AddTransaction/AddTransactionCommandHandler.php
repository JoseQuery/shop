<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Transaction\Application\Command\AddTransaction;

use ShoppingCart\Module\Cart\Domain\CartId;
use ShoppingCart\Module\Cart\Domain\CartRepository;
use ShoppingCart\Module\Cart\Domain\Product\Exception\ProductAmountDepleted;
use ShoppingCart\Module\Product\Domain\Amount;
use ShoppingCart\Module\Product\Domain\ProductId;
use ShoppingCart\Module\Product\Domain\ProductRepository;
use ShoppingCart\Module\Transaction\Domain\Transaction;
use ShoppingCart\Module\Transaction\Domain\TransactionProductId;
use ShoppingCart\Module\Transaction\Domain\TransactionRepository;
use ShoppingCart\Module\Transaction\Domain\TransactionSellerId;
use ShoppingCart\Module\Transaction\Domain\TransactionType;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

/**
 * Class AddTransactionCommandHandler
 * @package ShoppingCart\Module\Transaction\Application\Transaction\Command\AddProduct
 */
class AddTransactionCommandHandler implements MessageHandlerInterface
{
    /**
     * @var TransactionRepository
     */
    private $transactionRepository;
    /**
     * @var CartRepository
     */
    private $cartRepository;
    /**
     * @var ProductRepository
     */
    private $productRepository;

    public function __construct(
        TransactionRepository $transactionRepository,
        CartRepository $cartRepository,
        ProductRepository $productRepository
    ) {
        $this->transactionRepository = $transactionRepository;
        $this->cartRepository = $cartRepository;
        $this->productRepository = $productRepository;
    }

    /**
     * @param AddTransactionCommand $command
     */
    public function __invoke(AddTransactionCommand $command): void
    {
        $cart = $this->cartRepository->findOrFail(new CartId($command->cartId()));

        foreach ($cart->products()->toArray() as $cartProduct) {
            $transaction = new Transaction(
                $this->transactionRepository->nextId(),
                new TransactionProductId($cartProduct->productId()->value()),
                new TransactionSellerId($cartProduct->sellerId()->value()),
                new TransactionType(TransactionType::PURCHASE_TYPE)
            );

            $this->handleProduct($cartProduct);

            $this->transactionRepository->save($transaction);
        }
    }

    /**
     * @param \ShoppingCart\Module\Cart\Domain\Product\CartProduct $cartProduct
     */
    public function handleProduct(\ShoppingCart\Module\Cart\Domain\Product\CartProduct $cartProduct): void
    {
        $product = $this->productRepository->findOrFail(new ProductId($cartProduct->productId()->value()));
        if ($cartProduct->amount()->amount() > $product->amount()->amount()) {
            throw new ProductAmountDepleted($product->id()->value(), $product->amount()->amount());
        }
        if ($product->amount()->amount() - $cartProduct->amount()->amount() === 0) {
            $this->productRepository->delete($product);
            return;
        }
        $product->updateAmount(new Amount($product->amount()->amount() - $cartProduct->amount()->amount()));
        $this->productRepository->save($product);
    }
}