<?php

declare(strict_types=1);

namespace ShoppingCart\Module\Transaction\Infrastructure\Symfony\Bundle;

use ShoppingCart\Module\Transaction\Infrastructure\Symfony\DependencyInjection\TransactionExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class TransactionBundle extends Bundle
{
    protected function getContainerExtensionClass()
    {
        return TransactionExtension::class;
    }
}
