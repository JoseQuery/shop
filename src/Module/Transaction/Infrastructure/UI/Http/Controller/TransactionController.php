<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Transaction\Infrastructure\UI\Http\Controller;

use ShoppingCart\Module\Transaction\Application\Command\AddTransaction\AddTransactionCommand;
use Symfony\Component\Validator\Constraints as Assert;
use ShoppingCart\Module\Seller\Application\Command\AddSeller\AddSellerCommand;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use ShoppingCart\Common\Types\Infrastructure\Ui\Http\Rest\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class TransactionController extends Controller
{
    /**
     *
     * @Route("/confirm", name="post_transaction", methods={"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \ShoppingCart\Common\Types\Infrastructure\Ui\Http\Rest\Exception\BadFormatException
     */
    public function postSeller(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $this->validateTransaction($data);
        $this->dispatchCommand(
            new AddTransactionCommand(
                $data['cart_id']
            )
        );

        return $this->buildResponseForOk();
    }

    /**
     * @param array $seller
     * @throws \ShoppingCart\Common\Types\Infrastructure\Ui\Http\Rest\Exception\BadFormatException
     */
    private function validateTransaction(array $seller) : void
    {
        $contractConstraint = new Assert\Collection([
            'cart_id' => new Assert\NotBlank()
        ]);

        $this->validate($contractConstraint, $seller);
    }
}
