<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Transaction\Infrastructure\Persistence\Doctrine\Type;

use ShoppingCart\Common\Types\Infrastructure\Persistence\Doctrine\Type\DoctrineId;
use ShoppingCart\Module\Transaction\Domain\TransactionId;
use ShoppingCart\Module\Transaction\Domain\TransactionProductId;

/**
 * Class TransactionId
 * @package ShoppingCart\Module\Transaction\Infrastructure\Persistence\Doctrine\Type
 */
class DoctrineTransactionProductId extends DoctrineId
{
    public function className(): string
    {
        return TransactionProductId::class;
    }
}