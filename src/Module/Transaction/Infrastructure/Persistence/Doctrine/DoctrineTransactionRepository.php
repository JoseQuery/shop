<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Transaction\Infrastructure\Persistence\Doctrine;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Ramsey\Uuid\Uuid;
use ShoppingCart\Module\Transaction\Domain\Exception\ProductAlreadyExistsException;
use ShoppingCart\Module\Transaction\Domain\Exception\TransactionNotFoundException;
use ShoppingCart\Module\Transaction\Domain\Transaction;
use ShoppingCart\Module\Transaction\Domain\TransactionId;
use ShoppingCart\Module\Transaction\Domain\TransactionRepository;

class DoctrineTransactionRepository implements TransactionRepository
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Transaction $transaction
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(Transaction $transaction): void
    {
        try {
            $this->entityManager->persist($transaction);
            $this->entityManager->flush($transaction);
        } catch (UniqueConstraintViolationException $e) {
            throw new ProductAlreadyExistsException($transaction->id());
        }
    }

    /**
     * @param Transaction $transaction
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function delete(Transaction $transaction): void
    {
        $this->entityManager->remove($transaction);
        $this->entityManager->flush($transaction);
    }

    /**
     * @return TransactionId
     */
    public function nextId(): TransactionId
    {
        return new TransactionId(Uuid::uuid4()->toString());
    }

    public function findOrFail(TransactionId $transactionId): Transaction
    {
        $transaction = $this->entityManager->getRepository(Transaction::class)->find($transactionId);
        if ($transaction === null) {
            throw new TransactionNotFoundException($transactionId);
        }

        return $transaction;
    }
}
