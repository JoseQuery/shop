<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Transaction\Domain;

use ShoppingCart\Common\Types\Domain\Uuid;

class TransactionSellerId extends Uuid
{

}