<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Transaction\Domain\Exception;

use ShoppingCart\Common\Types\Domain\Exception\DomainException;

/**
 * Class InvalidTransactionType
 * @package ShoppingCart\Module\Transaction\Domain\Exception
 */
class InvalidTransactionType extends DomainException
{
    /**
     * @var string
     */
    private $type;

    /**
     * InvalidTransactionType constructor.
     * @param string $type
     */
    public function __construct(string $type)
    {
        $this->type = $type;
        parent::__construct();
    }

    /**
     * @return string
     */
    protected function errorMessage(): string
    {
        return 'Invalid transaction type: ' . $this->type;
    }
}