<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Transaction\Domain\Exception;


use ShoppingCart\Common\Types\Domain\Exception\DomainException;
use ShoppingCart\Module\Transaction\Domain\TransactionId;

class TransactionNotFoundException extends DomainException
{
    /**
     * @var TransactionId
     */
    private $transactionId;

    public function __construct(TransactionId $transactionId)
    {
        $this->transactionId = $transactionId;

        parent::__construct();
    }

    protected function errorMessage(): string
    {
        return "Transaction not found with id: {$this->transactionId->value()} does not exists";
    }
}