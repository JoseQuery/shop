<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Transaction\Domain;

use ShoppingCart\Common\Types\Domain\Uuid;

/**
 * Class TransactionId
 * @package ShoppingCart\Module\Transaction\Domain
 */
class TransactionId extends Uuid
{
}