<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Transaction\Domain;

/**
 * Class Transaction
 *
 * @package ShoppingCart\Module\Transaction\Domain
 */
class Transaction
{
    /**
     * @var TransactionId
     */
    private $id;
    /**
     * @var \DateTimeImmutable
     */
    private $createdAt;
    /**
     * @var TransactionProductId
     */
    private $productId;
    /**
     * @var TransactionSellerId
     */
    private $sellerId;
    /**
     * @var TransactionType
     */
    private $transactionType;

    /**
     * Transaction constructor.
     * @param TransactionId $id
     * @param TransactionProductId $productId
     * @param TransactionSellerId $sellerId
     * @param TransactionType $transactionType
     */
    public function __construct(
        TransactionId $id,
        TransactionProductId $productId,
        TransactionSellerId $sellerId,
        TransactionType $transactionType
    ) {
        $this->id = $id;
        $this->createdAt = new \DateTimeImmutable();
        $this->productId = $productId;
        $this->sellerId = $sellerId;
        $this->transactionType = $transactionType;
    }

    public function id() : TransactionId
    {
        return $this->id;
    }
}
