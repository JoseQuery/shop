<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Transaction\Domain;

use ShoppingCart\Common\Types\Domain\ValueObject;
use ShoppingCart\Module\Transaction\Domain\Exception\InvalidTransactionType;

class TransactionType extends ValueObject
{
    public const PURCHASE_TYPE = 'purchase';
    public const ADDED_TO_CART_TYPE = 'added_to_cart';

    private const ALLOWED_TYPES = [self::ADDED_TO_CART_TYPE, self::PURCHASE_TYPE];

    /**
     * @var string
     */
    private $type;

    /**
     * TransactionType constructor.
     * @param string $type
     */
    public function __construct(string $type)
    {
        $this->validate($type);
        $this->type = $type;
    }

    public function type() : string
    {
        return $this->type;
    }

    protected function equalValues(ValueObject $o): bool
    {
        return $this->type() === $o->value();
    }

    private function validate(string $type)
    {
        if (!in_array($type, self::ALLOWED_TYPES)) {
            throw new InvalidTransactionType($type);
        }
    }
}