<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Transaction\Domain;

use ShoppingCart\Module\Transaction\Domain\Exception\ProductAlreadyExistsException;
use ShoppingCart\Module\Transaction\Domain\Exception\TransactionNotFoundException;

interface TransactionRepository
{
    /**
     * @return TransactionId
     */
    public function nextId(): TransactionId;

    /**
     * @param Transaction $lender
     *
     * @throws ProductAlreadyExistsException
     */
    public function save(Transaction $lender): void;

    /**
     * @param Transaction $transaction
     *
     * @throws TransactionNotFoundException
     */
    public function delete(Transaction $transaction): void;

    /**
     * @param TransactionId $transactionId
     * @return Transaction
     */
    public function findOrFail(TransactionId $transactionId) : Transaction;
}
