<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Cart\Application\Query\GetTotalPrice;

use ShoppingCart\Common\Types\Application\DtoResponse;
use ShoppingCart\Module\Cart\Domain\Product\CartProductPrice;

class TotalPriceDto implements DtoResponse
{
    /**
     * @var CartProductPrice
     */
    private $totalPrice;

    /**
     * AmountDto constructor.
     * @param CartProductPrice $totalPrice
     */
    public function __construct(CartProductPrice $totalPrice)
    {
        $this->totalPrice = $totalPrice;
    }

    /**
     * @return array|CartProductPrice[]
     */
    public function toArray(): array
    {
        return [
            'total_price' => $this->totalPrice->price()
        ];
    }
}