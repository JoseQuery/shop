<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Cart\Application\Query\GetTotalPrice;

use ShoppingCart\Common\Types\Application\QueryBus\Query;

/**
 * Class DeleteProductCommand
 * @package ShoppingCart\Module\Product\Application\Command\DeleteProduct
 */
class GetTotalPriceQuery implements Query
{
    /**
     * @var string
     */
    private $cart;

    /**
     * DeleteProductCommand constructor.
     * @param string $cartId
     */
    public function __construct(string $cartId)
    {
        $this->cart = $cartId;
    }

    /**
     * @return string
     */
    public function cartId(): string
    {
        return $this->cart;
    }
}