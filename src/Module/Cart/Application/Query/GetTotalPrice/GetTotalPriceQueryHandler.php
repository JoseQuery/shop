<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Cart\Application\Query\GetTotalPrice;

use ShoppingCart\Common\Types\Application\DtoResponse;
use ShoppingCart\Module\Cart\Domain\CartId;
use ShoppingCart\Module\Cart\Domain\CartRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

/**
 * Class DeleteProductCommand
 * @package ShoppingCart\Module\Product\Application\Command\DeleteProduct
 */
class GetTotalPriceQueryHandler implements MessageHandlerInterface
{
    /**
     * @var CartRepository
     */
    private $cartRepository;

    /**
     * DeleteCartCommandHandler constructor.
     *
     * @param CartRepository $sellerRepository
     */
    public function __construct(CartRepository $sellerRepository)
    {
        $this->cartRepository = $sellerRepository;
    }

    /**
     * @param GetTotalPriceQuery $command
     * @return DtoResponse
     */
    public function __invoke(GetTotalPriceQuery $command): DtoResponse
    {
        $cart = $this->cartRepository->findOrFail(new CartId($command->cartId()));

        return new TotalPriceDto($cart->totalPriceAmount());
    }
}