<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Cart\Application\Command\AddProductToCart;

use ShoppingCart\Module\Cart\Domain\Cart;
use ShoppingCart\Module\Cart\Domain\CartId;
use ShoppingCart\Module\Cart\Domain\CartProductCollection;
use ShoppingCart\Module\Cart\Domain\CartRepository;
use ShoppingCart\Module\Cart\Domain\Exception\UnableToAddProductRepeated;
use ShoppingCart\Module\Cart\Domain\Product\CartProduct;
use ShoppingCart\Module\Cart\Domain\Product\CartProductAmount;
use ShoppingCart\Module\Cart\Domain\Product\CartProductPrice;
use ShoppingCart\Module\Cart\Domain\Product\Exception\ProductAmountDepleted;
use ShoppingCart\Module\Cart\Domain\Product\SellerId;
use ShoppingCart\Module\Product\Domain\Product;
use ShoppingCart\Module\Product\Domain\ProductId;
use ShoppingCart\Module\Product\Domain\ProductRepository;
use ShoppingCart\Module\Transaction\Domain\Transaction;
use ShoppingCart\Module\Transaction\Domain\TransactionProductId;
use ShoppingCart\Module\Transaction\Domain\TransactionRepository;
use ShoppingCart\Module\Transaction\Domain\TransactionSellerId;
use ShoppingCart\Module\Transaction\Domain\TransactionType;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

/**
 * Class AddProductCommandHandler
 * @package ShoppingCart\Module\Product\Application\Product\Command\AddProduct
 */
class AddProductToCartCommandHandler implements MessageHandlerInterface
{
    /**
     * @var CartRepository
     */
    private $cartRepository;
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var TransactionRepository
     */
    private $transactionRepository;

    /**
     * AddProductCommandHandler constructor.
     * @param CartRepository $cartRepository
     * @param ProductRepository $productRepository
     * @param TransactionRepository $transactionRepository
     */
    public function __construct(
        CartRepository $cartRepository,
        ProductRepository $productRepository,
        TransactionRepository $transactionRepository
    )
    {
        $this->cartRepository = $cartRepository;
        $this->productRepository = $productRepository;
        $this->transactionRepository = $transactionRepository;
    }

    /**
     * @param AddProductToCartCommand $command
     */
    public function __invoke(AddProductToCartCommand $command): void
    {
        $products = [];
        $productIds = [];
        foreach ($command->products() as $cartProductArray) {
            if ($cartProductArray['product_amount'] === 0) {
                continue;
            }
            $product = $this->validateProduct($cartProductArray, $productIds);
            $products[] = new CartProduct(
                new SellerId($product->sellerId()->value()),
                new CartProductPrice($product->price()->price()),
                new CartProductAmount($cartProductArray['product_amount']),
                new \ShoppingCart\Module\Cart\Domain\Product\ProductId($product->id()->value())
            );

            $productIds[] = $product->id()->value();
        }

        $this->manageCart($command, $products);
        $this->addTransactions($products);
    }

    /**
     * @param $cartProductArray
     * @param array $productIds
     * @return Product
     */
    public function validateProduct($cartProductArray, array $productIds): Product
    {
        if (in_array($cartProductArray['product_id'], $productIds)) {
            throw new UnableToAddProductRepeated($cartProductArray['product_id']);
        }
        $product = $this->productRepository->findOrFail(new ProductId($cartProductArray['product_id']));
        if ($cartProductArray['product_amount'] > $product->amount()->amount()) {
            throw new ProductAmountDepleted($product->id()->value(), $product->amount()->amount());
        }
        return $product;
    }

    /**
     * @param AddProductToCartCommand $command
     * @param array $products
     */
    public function manageCart(AddProductToCartCommand $command, array $products): void
    {
        /** @var Cart $cart */
        $cart = $this->cartRepository->find(new CartId($command->cartId()));
        if ($cart === null) {
            if (empty($products)) {
                return;
            }
            $cart = new Cart(new CartId($command->cartId()), new CartProductCollection($products));
            $this->cartRepository->save($cart);
            return;
        }

        if (empty($products)) {
            $this->cartRepository->delete($cart);
        }

        $cart->updateProductCollection(new CartProductCollection($products));
        $this->cartRepository->save($cart);
    }

    /**
     * @param array $products
     */
    private function addTransactions(array $products)
    {
        foreach ($products as $product) {
            $transaction = new Transaction(
                $this->transactionRepository->nextId(),
                new TransactionProductId($product->productId()->value()),
                new TransactionSellerId($product->sellerId()->value()),
                new TransactionType(TransactionType::ADDED_TO_CART_TYPE)
            );
            $this->transactionRepository->save($transaction);
        }

    }
}
