<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Cart\Application\Command\AddProductToCart;

use ShoppingCart\Common\Types\Application\CommandBus\Command;

/**
 * Class AddProductCommand
 * @package ShoppingCart\Module\Product\Application\Product\Command\AddProduct
 */
class AddProductToCartCommand implements Command
{
    /**
     * @var string
     */
    private $cartId;
    /**
     * @var array
     */
    private $products;

    /**
     * AddProductToCartCommand constructor.
     * @param string $cartId
     * @param array $products
     */
    public function __construct(string $cartId, array $products)
    {
        $this->cartId = $cartId;
        $this->products = $products;
    }

    /**
     * @return string
     */
    public function cartId(): string
    {
        return $this->cartId;
    }

    /**
     * @return array
     */
    public function products(): array
    {
        return $this->products;
    }
}
