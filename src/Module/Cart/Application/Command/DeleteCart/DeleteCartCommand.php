<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Cart\Application\Command\DeleteCart;

use ShoppingCart\Common\Types\Application\CommandBus\Command;

/**
 * Class DeleteProductCommand
 * @package ShoppingCart\Module\Product\Application\Command\DeleteProduct
 */
class DeleteCartCommand implements Command
{
    /**
     * @var string
     */
    private $cartId;

    /**
     * DeleteProductCommand constructor.
     * @param string $cartId
     */
    public function __construct(string $cartId)
    {
        $this->cartId = $cartId;
    }

    /**
     * @return string
     */
    public function cartId(): string
    {
        return $this->cartId;
    }
}