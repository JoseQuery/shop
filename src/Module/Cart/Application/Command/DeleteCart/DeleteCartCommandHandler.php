<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Cart\Application\Command\DeleteCart;

use ShoppingCart\Module\Cart\Domain\CartId;
use ShoppingCart\Module\Cart\Domain\CartRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

/**
 * Class DeleteProductCommandHandler
 * @package ShoppingCart\Module\Product\Application\Command\DeleteProduct
 */
class DeleteCartCommandHandler implements MessageHandlerInterface
{
    /**
     * @var CartRepository
     */
    private $cartRepository;

    /**
     * DeleteCartCommandHandler constructor.
     *
     * @param CartRepository $sellerRepository
     */
    public function __construct(CartRepository $sellerRepository)
    {
        $this->cartRepository = $sellerRepository;
    }

    /**
     * @param DeleteCartCommand $command
     */
    public function __invoke(DeleteCartCommand $command): void
    {
        $cart = $this->cartRepository->findOrFail(new CartId($command->cartId()));
        $this->cartRepository->delete($cart);
    }
}