<?php

declare(strict_types=1);

namespace ShoppingCart\Module\Cart\Infrastructure\Symfony\Bundle;

use ShoppingCart\Module\Cart\Infrastructure\Symfony\DependencyInjection\CartExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class CartBundle extends Bundle
{
    protected function getContainerExtensionClass()
    {
        return CartExtension::class;
    }
}
