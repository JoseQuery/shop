<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Cart\Infrastructure\UI\Http\Controller;

use \Exception;

use ShoppingCart\Common\Types\Infrastructure\Ui\Http\Rest\Exception\BadFormatException;
use ShoppingCart\Module\Cart\Application\Command\DeleteCart\DeleteCartCommand;
use ShoppingCart\Module\Cart\Application\Query\GetTotalPrice\GetTotalPriceQuery;
use Symfony\Component\Validator\Constraints as Assert;
use ShoppingCart\Module\Cart\Application\Command\AddProductToCart\AddProductToCartCommand;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use ShoppingCart\Common\Types\Infrastructure\Ui\Http\Rest\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class CartController extends Controller
{
    /**
     *
     * @Route("/cart", name="post_cart", methods={"PUT"})
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \ShoppingCart\SharedKernel\Infrastructure\Ui\Http\Rest\Exception\BadFormatException
     */
    public function addCart(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $this->validateContract($data);

        $this->dispatchCommand(new AddProductToCartCommand(
            $data['cart_id'],
            $data['products']
        ));

        return $this->buildResponseForOk();
    }

    /**
     *
     * @Route("/cart/{cartId}", name="delete_cart", methods={"DELETE"})
     *
     * @param string $cartId
     * @return JsonResponse
     */
    public function deleteSeller(string $cartId): JsonResponse
    {
        $this->dispatchCommand(new DeleteCartCommand($cartId));

        return $this->buildResponseForOk();
    }

    /**
     *
     * @Route("/cart/price/{cartId}", name="get_total_amount", methods={"GET"})
     *
     * @param string $cartId
     * @return JsonResponse
     */
    public function getTotalAmount(string $cartId)
    {
        $dtoResponse = $this->dispatchQuery(new GetTotalPriceQuery($cartId));

        return $this->buildResponseForOk($dtoResponse->toArray());
    }

    /**
     * @param array $contract
     *
     * @throws BadFormatException
     */
    private function validateContract(array $contract)
    {
        $contractConstraint = new Assert\Collection([
            'cart_id' => new Assert\NotBlank(),
            'products' => new Assert\NotBlank()
        ]);

        $this->validate($contractConstraint, $contract);
    }
}

