<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Cart\Infrastructure\Persistence\Doctrine;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Ramsey\Uuid\Uuid;
use ShoppingCart\Module\Cart\Domain\Exception\CardAlreadyExistsException;
use ShoppingCart\Module\Cart\Domain\Cart;
use ShoppingCart\Module\Cart\Domain\CartId;
use ShoppingCart\Module\Cart\Domain\CartRepository;
use ShoppingCart\Module\Cart\Domain\Exception\CartNotFoundException;

/**
 * Class DoctrineCartRepository
 * @package ShoppingCart\Module\Cart\Infrastructure\Persistence\Doctrine
 */
class DoctrineCartRepository implements CartRepository
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * DoctrineCartRepository constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Cart $cart
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(Cart $cart): void
    {
        // TODO i should see if products exists before adding them
        // $this->assertProductsExist($cart->productIds());
        try {
            $this->entityManager->persist($cart);
            $this->entityManager->flush($cart);
        } catch (UniqueConstraintViolationException $e) {
            throw new CardAlreadyExistsException($cart->id());
        }
    }

    /**
     * @param Cart $Cart
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function delete(Cart $Cart): void
    {
        $this->entityManager->remove($Cart);
        $this->entityManager->flush($Cart);
    }

    /**
     * @return CartId
     */
    public function nextId(): CartId
    {
        return new CartId(Uuid::uuid4()->toString());
    }

    /**
     * @param CartId $CartId
     * @return Cart
     */
    public function find(CartId $CartId):? Cart
    {
        return $this->entityManager->getRepository(Cart::class)->find($CartId);
    }

    /**
     * @param CartId $cartId
     * @return Cart
     */
    public function findOrFail(CartId $cartId): Cart
    {
        $cart = $this->entityManager->getRepository(Cart::class)->find($cartId);
        if ($cart === null) {
            throw new CartNotFoundException($cartId);
        }
        return $cart;
    }
}
