<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Cart\Infrastructure\Persistence\Doctrine\Type;

use ShoppingCart\Common\Types\Infrastructure\Persistence\Doctrine\Type\DoctrineId;
use ShoppingCart\Module\Cart\Domain\CartId;

/**
 * Class SellerId
 * @package ShoppingCart\Module\Seller\Infrastructure\Persistence\Doctrine\Type
 */
class DoctrineCartId extends DoctrineId
{
    public function className(): string
    {
        return CartId::class;
    }
}