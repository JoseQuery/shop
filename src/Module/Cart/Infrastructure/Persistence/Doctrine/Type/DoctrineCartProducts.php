<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Cart\Infrastructure\Persistence\Doctrine\Type;


use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use ShoppingCart\Module\Cart\Domain\Product\CartProduct;
use ShoppingCart\Module\Cart\Domain\CartProductCollection;
use ShoppingCart\Module\Cart\Domain\Product\CartProductAmount;
use ShoppingCart\Module\Cart\Domain\Product\CartProductId;
use ShoppingCart\Module\Cart\Domain\Product\CartProductPrice;
use ShoppingCart\Module\Cart\Domain\Product\ProductId;
use ShoppingCart\Module\Cart\Domain\Product\SellerId;

class DoctrineCartProducts extends Type
{
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        /** @var CartProductCollection $value */
        $array = array_map(function (CartProduct $product) {
            return json_encode($product->toArray());
        }, $value->toArray());

        return json_encode($array);
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $cartProductsJsons = json_decode($value);
        $products = [];
        foreach ($cartProductsJsons as $productJson) {
            $productArray = json_decode($productJson, true);
            $products[] = new CartProduct(
              new SellerId($productArray['seller_id']),
              new CartProductPrice($productArray['price']),
              new CartProductAmount($productArray['amount']),
              new ProductId($productArray['product_id'])
            );
        }
        return new CartProductCollection($products);
    }

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return 'Longtext';
    }

    public function getName()
    {
        return CartProductCollection::class;
    }
}