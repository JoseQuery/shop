<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Cart\Domain;

use ShoppingCart\Common\Types\Domain\Uuid;

/**
 * Class CartId
 * @package ShoppingCart\Module\Cart\Domain
 */
class CartId extends Uuid
{

}