<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Cart\Domain;

use ShoppingCart\Module\Cart\Domain\Exception\ProductIdCollectionMustContainOnlyProductIds;
use ShoppingCart\Module\Cart\Domain\Product\CartProduct;

/**
 * Class ProductIdCollection
 * @package ShoppingCart\Module\Seller\Domain
 */
class CartProductCollection
{
    /**
     * @var CartProduct[]
     */
    private $products;

    /**
     * ProductIdCollection constructor.
     * @param CartProduct[] $products
     */
    public function __construct(array $products)
    {
        foreach ($products as $productId) {
            if (!$productId instanceof CartProduct) {
                throw new ProductIdCollectionMustContainOnlyProductIds();
            }
        }
        $this->products = $products;
    }

    public function toArray(): array
    {
        return $this->products;
    }

}
