<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Cart\Domain;

use ShoppingCart\Module\Cart\Domain\Exception\CardAlreadyExistsException;
use ShoppingCart\Module\Cart\Domain\Exception\CartNotFoundException;

/**
 * Interface CartRepository
 * @package ShoppingCart\Module\Cart\Domain
 */
interface CartRepository
{
    /**
     * @return CartId
     */
    public function nextId(): CartId;

    /**
     * @param Cart $cart
     *
     * @throws CardAlreadyExistsException
     */
    public function save(Cart $cart): void;

    /**
     * @param Cart $cart
     *
     * @throws CartNotFoundException
     */
    public function delete(Cart $cart): void;

    /**
     * @param CartId $cartId
     * @return Cart
     */
    public function find(CartId $cartId):? Cart;

    /**
     * @param CartId $cartId
     * @return Cart
     */
    public function findOrFail(CartId $cartId): Cart;
}
