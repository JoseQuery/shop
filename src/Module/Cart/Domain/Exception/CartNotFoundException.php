<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Cart\Domain\Exception;

use ShoppingCart\Common\Types\Domain\Exception\DomainException;
use ShoppingCart\Module\Cart\Domain\CartId;

class CartNotFoundException extends DomainException
{
    /**
     * @var CartId
     */
    private $cartId;

    /**
     * ProductNotFoundException constructor.
     * @param CartId $cartId
     */
    public function __construct(CartId $cartId)
    {
        $this->cartId = $cartId;

        parent::__construct();
    }

    /**
     * @return string
     */
    protected function errorMessage(): string
    {
        return "Cart not found with id: {$this->cartId->value()} does not exists";
    }
}