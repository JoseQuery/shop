<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Cart\Domain\Exception;

use ShoppingCart\Common\Types\Domain\Exception\DomainException;
use ShoppingCart\Module\Cart\Domain\CartId;
use ShoppingCart\Module\Product\Domain\ProductId;
use ShoppingCart\Module\Seller\Domain\SellerId;

class CardAlreadyExistsException extends DomainException
{
    /**
     * @var CartId
     */
    private $cartId;

    public function __construct(CartId $cartId)
    {
        $this->cartId = $cartId;

        parent::__construct();
    }

    protected function errorMessage(): string
    {
        return "Cart with id: {$this->cartId->value()} already exists";
    }
}
