<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Cart\Domain\Exception;

use ShoppingCart\Common\Types\Domain\Exception\DomainException;

class UnableToAddProductRepeated extends DomainException
{
    /**
     * @var string
     */
    private $id;

    public function __construct(string $id)
    {
        $this->id = $id;
        parent::__construct();
    }

    protected function errorMessage(): string
    {
        return 'Unable to add product repeated with id ' . $this->id;
    }
}