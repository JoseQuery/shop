<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Cart\Domain\Exception;

use ShoppingCart\Common\Types\Domain\Exception\DomainException;

/**
 * Class ProductIdCollectionMustContainOnlyProductIds
 * @package ShoppingCart\Module\Seller\Domain\Exception
 */
class ProductIdCollectionMustContainOnlyProductIds extends DomainException
{
    protected function errorMessage(): string
    {
        return "ProductIdCollection must contain only product ids";
    }
}
