<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Cart\Domain\Product;

use ShoppingCart\Common\Types\Domain\ValueObject;
use ShoppingCart\Module\Cart\Domain\Product\Exception\PriceCannotBeNegative;
use ShoppingCart\Module\Seller\Domain\Exception\NameCannotBeEmpty;

/**
 * Class PartName
 * @package ShoppingCart\Module\Contract\Domain\Parts
 */
class CartProductPrice extends ValueObject
{
    /**
     * @var float
     */
    private $price;

    /**
     * Name constructor.
     *
     * @param float $price
     * @throws NameCannotBeEmpty
     */
    public function __construct(float $price)
    {
        $this->assertValue($price);
        $this->price = $price;
    }

    /**
     * @return float
     */
    public function price(): float
    {
        return $this->price;
    }

    protected function equalValues(ValueObject $o): bool
    {
        return $this->price() === $o->value();
    }

    /**
     * @param float $price
     * @throws NameCannotBeEmpty
     */
    private function assertValue(float $price)
    {
        if ($price < 0.0) {
            throw new PriceCannotBeNegative();
        }
    }
}
