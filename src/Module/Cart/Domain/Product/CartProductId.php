<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Cart\Domain\Product;

use ShoppingCart\Common\Types\Domain\Uuid;

/**
 * Class SellerId
 * @package ShoppingCart\Module\Seller\Domain
 */
class CartProductId extends Uuid
{
}
