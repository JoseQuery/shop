<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Cart\Domain\Product\Exception;

use ShoppingCart\Common\Types\Domain\Exception\DomainException;
use ShoppingCart\Module\Cart\Domain\Product\CartProductId;

/**
 * Class ProductAlreadyExistsException
 *
 * @package ShoppingCart\Module\Product\Domain\Exception
 */
class ProductAlreadyExistsException extends DomainException
{
    /**
     * @var CartProductId
     */
    private $productId;

    /**
     * ProductAlreadyExistsException constructor.
     * @param CartProductId $productId
     */
    public function __construct(CartProductId $productId)
    {
        $this->productId = $productId;

        parent::__construct();
    }

    /**
     * @return string
     */
    protected function errorMessage(): string
    {
        return "Product with id: {$this->productId->value()} already exists";
    }
}
