<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Cart\Domain\Product\Exception;

use ShoppingCart\Common\Types\Domain\Exception\DomainException;

/**
 * Class PartyNameCannotBeEmpty
 * @package ShoppingCart\Module\Contract\Domain\Exception
 */
class AmountCannotBeNegative extends DomainException
{
    /**
     * @return string
     */
    protected function errorMessage(): string
    {
        return 'Amount of products cannot be 0 or negative';
    }
}
