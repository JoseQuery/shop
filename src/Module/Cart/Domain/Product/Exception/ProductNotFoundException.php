<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Cart\Domain\Product\Exception;

use ShoppingCart\Common\Types\Domain\Exception\DomainException;
use ShoppingCart\Module\Cart\Domain\Product\CartProductId;

class ProductNotFoundException extends DomainException
{
    /**
     * @var CartProductId
     */
    private $productId;

    /**
     * ProductNotFoundException constructor.
     * @param CartProductId $productId
     */
    public function __construct(CartProductId $productId)
    {
        $this->productId = $productId;

        parent::__construct();
    }

    protected function errorMessage(): string
    {
        return "Product not found with id: {$this->productId->value()} does not exists";
    }
}