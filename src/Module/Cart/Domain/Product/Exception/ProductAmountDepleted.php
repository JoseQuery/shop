<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Cart\Domain\Product\Exception;

use ShoppingCart\Common\Types\Domain\Exception\DomainException;

/**
 * Class ProductAmountDepleted
 * @package ShoppingCart\Module\Cart\Domain\Product\Exception
 */
class ProductAmountDepleted extends DomainException
{
    /**
     * @var string
     */
    private $productId;
    /**
     * @var int
     */
    private $unitsRemaining;

    /**
     * ProductAmountDepleted constructor.
     * @param string $productId
     * @param int $unitsRemaining
     */
    public function __construct(string $productId, int $unitsRemaining)
    {
        $this->productId = $productId;
        $this->unitsRemaining = $unitsRemaining;
        parent::__construct();
    }

    protected function errorMessage(): string
    {
        return 'The stock of products with id: ' .$this->productId . 'almost depleted, units remaining: ' . $this->unitsRemaining;
    }
}
