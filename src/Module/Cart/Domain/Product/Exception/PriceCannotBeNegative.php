<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Cart\Domain\Product\Exception;

use ShoppingCart\Common\Types\Domain\Exception\DomainException;

/**
 * Class PartyNameCannotBeEmpty
 * @package ShoppingCart\Module\Contract\Domain\Exception
 */
class PriceCannotBeNegative extends DomainException
{
    /**
     * @return string
     */
    protected function errorMessage(): string
    {
        return 'Price can not be negative';
    }
}