<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Cart\Domain\Product;

/**
 * Class Product
 * @package ShoppingCart\Module\Product
 */
class CartProduct
{
    /**
     * @var SellerId
     */
    private $sellerId;
    /**
     * @var \DateTimeImmutable $createdAt
     */
    private $createdAt;
    /**
     * @var CartProductPrice
     */
    private $price;
    /**
     * @var CartProductAmount
     */
    private $amount;
    /**
     * @var CartProductId|ProductId
     */
    private $productId;

    /**
     * Product constructor.
     * @param SellerId $sellerId
     * @param CartProductPrice $price
     * @param CartProductAmount $cartProductAmount
     * @param ProductId $productId
     */
    public function __construct(
        SellerId $sellerId,
        CartProductPrice $price,
        CartProductAmount $cartProductAmount,
        ProductId $productId
    ) {
        $this->sellerId = $sellerId;
        $this->createdAt = new \DateTimeImmutable();
        $this->price = $price;
        $this->amount = $cartProductAmount;
        $this->productId = $productId;
    }

    /**
     * @return SellerId
     */
    public function sellerId(): SellerId
    {
        return $this->sellerId;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function createdAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @return CartProductPrice
     */
    public function price(): CartProductPrice
    {
        return $this->price;
    }

    /**
     * @return CartProductAmount
     */
    public function amount(): CartProductAmount
    {
        return $this->amount;
    }

    /**
     * @return CartProductId|ProductId
     */
    public function productId()
    {
        return $this->productId;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'seller_id' => $this->sellerId->value(),
            'created_at' => $this->createdAt->format(DATE_ATOM),
            'price' => $this->price->price(),
            'amount' => $this->amount->amount(),
            'product_id' => $this->productId->value()
        ];
    }
}