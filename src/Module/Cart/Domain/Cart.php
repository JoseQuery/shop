<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Cart\Domain;

use ShoppingCart\Module\Cart\Application\Query\GetTotalPrice\TotalPriceDto;
use ShoppingCart\Module\Cart\Domain\Product\CartProductPrice;

/**
 * Class Cart
 * @package ShoppingCart\Module\Cart\Domain\Cart
 */
class Cart
{
    /**
     * @var CartId
     */
    private $id;

    /**
     * @var \DateTimeImmutable $createdAt
     */
    private $createdAt;

    /**
     * @var CartProductCollection
     */
    private $products;

    /**
     * Cart constructor.
     * @param CartId $cartId
     * @param CartProductCollection $productIdCollection
     */
    public function __construct(CartId $cartId, ?CartProductCollection $productIdCollection = null)
    {
        $this->id = $cartId;
        $this->products = $productIdCollection;
        $this->createdAt = new \DateTimeImmutable();
    }

    /**
     * @return CartId
     */
    public function id(): CartId
    {
        return $this->id;
    }

    /**
     * @param CartProductCollection $products
     */
    public function updateProductCollection(CartProductCollection $products)
    {
        $this->products = $products;
    }

    public function products()
    {
        return $this->products;
    }

    /**
     *
     */
    public function totalPriceAmount() : CartProductPrice
    {
        $totalPrice = 0;
        foreach ($this->products->toArray() as $product) {
            $totalPrice += $product->amount()->amount() * $product->price()->price();
        }

        return new CartProductPrice($totalPrice);
    }
}
