<?php

declare(strict_types=1);

namespace ShoppingCart\Module\Product\Infrastructure\Symfony\Bundle;

use ShoppingCart\Module\Product\Infrastructure\Symfony\DependencyInjection\ProductExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class ProductBundle extends Bundle
{
    protected function getContainerExtensionClass()
    {
        return ProductExtension::class;
    }
}
