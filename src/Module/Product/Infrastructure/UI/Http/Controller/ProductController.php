<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Product\Infrastructure\UI\Http\Controller;

use ShoppingCart\Module\Product\Application\Command\DeleteProduct\DeleteProductCommand;
use Symfony\Component\Validator\Constraints as Assert;
use ShoppingCart\Common\Types\Infrastructure\Ui\Http\Rest\Controller\Controller;
use ShoppingCart\Module\Product\Application\Command\AddProduct\AddProductCommand;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class ProductController
 * @package ShoppingCart\Module\Product\Infrastructure\UI\Http\Controller
 */
class ProductController extends Controller
{
    /**
     *
     * @Route("/product", name="post_product", methods={"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \ShoppingCart\Common\Types\Infrastructure\Ui\Http\Rest\Exception\BadFormatException
     */
    public function postProduct(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $this->validateProduct($data);
        $resourceId = $this->dispatchCommand(
            new AddProductCommand(
                $data['product_name'],
                $data['seller_id'],
                (float) $data['product_price'],
                (int) $data['product_amount']
            )
        );

        return $this->buildResponseForOk($resourceId);
    }

    /**
     *
     * @Route("/product/{productId}", name="delete_product", methods={"DELETE"})
     *
     * @param string $productId
     * @return JsonResponse
     */
    public function deleteProduct(string $productId): JsonResponse
    {
        $this->dispatchCommand(new DeleteProductCommand($productId));

        return $this->buildResponseForOk();
    }

    /**
     * @param array $product
     * @throws \ShoppingCart\Common\Types\Infrastructure\Ui\Http\Rest\Exception\BadFormatException
     */
    private function validateProduct(array $product) : void
    {
        $contractConstraint = new Assert\Collection([
            'product_name' => new Assert\NotBlank(),
            'seller_id' => new Assert\NotBlank(),
            'product_price' => new Assert\NotNull(),
            'product_amount' => new Assert\NotNull()
        ]);

        $this->validate($contractConstraint, $product);
    }
}
