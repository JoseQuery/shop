<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Product\Infrastructure\Persistence\Doctrine\Type;

use ShoppingCart\Common\Types\Infrastructure\Persistence\Doctrine\Type\DoctrineId;
use ShoppingCart\Module\Product\Domain\SellerId;

/**
 * Class SellerId
 * @package ShoppingCart\Module\Seller\Infrastructure\Persistence\Doctrine\Type
 */
class DoctrineProductSellerId extends DoctrineId
{
    public function className(): string
    {
        return SellerId::class;
    }
}