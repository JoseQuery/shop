<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Product\Infrastructure\Persistence\Doctrine\Type;

use ShoppingCart\Common\Types\Infrastructure\Persistence\Doctrine\Type\DoctrineId;
use ShoppingCart\Module\Cart\Domain\CartId;
use ShoppingCart\Module\Product\Domain\ProductId;

/**
 * Class SellerId
 * @package ShoppingCart\Module\Seller\Infrastructure\Persistence\Doctrine\Type
 */
class DoctrineProductId extends DoctrineId
{
    public function className(): string
    {
        return ProductId::class;
    }
}