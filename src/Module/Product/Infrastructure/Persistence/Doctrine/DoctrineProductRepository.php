<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Product\Infrastructure\Persistence\Doctrine;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Ramsey\Uuid\Uuid;
use ShoppingCart\Module\Product\Domain\Exception\ProductAlreadyExistsException;
use ShoppingCart\Module\Product\Domain\Exception\ProductNotFoundException;
use ShoppingCart\Module\Product\Domain\Product;
use ShoppingCart\Module\Product\Domain\ProductId;
use ShoppingCart\Module\Product\Domain\ProductRepository;

class DoctrineProductRepository implements ProductRepository
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Product $product
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(Product $product): void
    {
        try {
            $this->entityManager->persist($product);
            $this->entityManager->flush($product);
        } catch (UniqueConstraintViolationException $e) {
            throw new ProductAlreadyExistsException($product->id());
        }
    }

    /**
     * @param Product $product
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function delete(Product $product): void
    {
        $this->entityManager->remove($product);
        $this->entityManager->flush($product);
    }

    /**
     * @return ProductId
     */
    public function nextId(): ProductId
    {
        return new ProductId(Uuid::uuid4()->toString());
    }

    public function findOrFail(ProductId $productId): Product
    {
        $product = $this->entityManager->getRepository(Product::class)->find($productId);
        if ($product === null) {
            throw new ProductNotFoundException($productId);
        }

        return $product;
    }
}
