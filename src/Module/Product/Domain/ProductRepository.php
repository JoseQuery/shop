<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Product\Domain;

use ShoppingCart\Module\Product\Domain\Exception\ProductAlreadyExistsException;

interface ProductRepository
{
    /**
     * @return ProductId
     */
    public function nextId(): ProductId;

    /**
     * @param Product $lender
     *
     * @throws ProductAlreadyExistsException
     */
    public function save(Product $lender): void;

    /**
     * @param Product $product
     *
     */
    public function delete(Product $product): void;

    /**
     * @param ProductId $productId
     * @return Product
     */
    public function findOrFail(ProductId $productId) : Product;
}
