<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Product\Domain;

use ShoppingCart\Common\Types\Domain\Uuid;

/**
 * Class SellerId
 * @package ShoppingCart\Module\Seller\Domain
 */
class ProductId extends Uuid
{
}
