<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Product\Domain;

use ShoppingCart\Common\Types\Domain\ValueObject;
use ShoppingCart\Module\Product\Domain\Exception\AmountCannotBeNegative;

/**
 * Class CartProductAmount
 * @package ShoppingCart\Module\Cart\Domain\Product
 */
class Amount extends ValueObject
{
    /**
     * @var int
     */
    private $amount;

    /**
     * Name constructor.
     *
     * @param int $amount
     * @throws AmountCannotBeNegative
     */
    public function __construct(int $amount)
    {
        $this->assertValue($amount);
        $this->amount = $amount;
    }

    /**
     * @return int
     */
    public function amount(): int
    {
        return $this->amount;
    }

    protected function equalValues(ValueObject $o): bool
    {
        return $this->amount() === $o->value();
    }

    /**
     * @param int $amount
     * @throws AmountCannotBeNegative
     */
    private function assertValue(int $amount)
    {
        if ($amount < 1) {
            throw new AmountCannotBeNegative();
        }
    }
}
