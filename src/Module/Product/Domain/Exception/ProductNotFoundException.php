<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Product\Domain\Exception;

use ShoppingCart\Common\Types\Domain\Exception\DomainException;
use ShoppingCart\Module\Product\Domain\ProductId;
use ShoppingCart\Module\Seller\Domain\SellerId;

class ProductNotFoundException extends DomainException
{
    /**
     * @var SellerId
     */
    private $productId;

    /**
     * ProductNotFoundException constructor.
     * @param ProductId $productId
     */
    public function __construct(ProductId $productId)
    {
        $this->productId = $productId;

        parent::__construct();
    }

    protected function errorMessage(): string
    {
        return "Product not found with id: {$this->productId->value()} does not exists";
    }
}