<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Product\Domain\Exception;

use ShoppingCart\Common\Types\Domain\Exception\DomainException;
use ShoppingCart\Module\Product\Domain\ProductId;
use ShoppingCart\Module\Seller\Domain\SellerId;

class ProductAlreadyExistsException extends DomainException
{
    /**
     * @var SellerId
     */
    private $productId;

    public function __construct(ProductId $productId)
    {
        $this->productId = $productId;

        parent::__construct();
    }

    protected function errorMessage(): string
    {
        return "Seller with id: {$this->productId->value()} already exists";
    }
}
