<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Product\Domain\Exception;

use ShoppingCart\Common\Types\Domain\Exception\DomainException;

/**
 * Class PartyNameCannotBeEmpty
 * @package ShoppingCart\Module\Contract\Domain\Exception
 */
class NameCannotBeEmpty extends DomainException
{
    /**
     * @return string
     */
    protected function errorMessage(): string
    {
        return 'Product name cannot be empty';
    }
}