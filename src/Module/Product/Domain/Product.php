<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Product\Domain;

/**
 * Class Product
 * @package ShoppingCart\Module\Product
 */
class Product
{
    /**
     * @var ProductId
     */
    private $id;
    /**
     * @var Name
     */
    private $name;
    /**
     * @var SellerId
     */
    private $sellerId;
    /**
     * @var \DateTimeImmutable $createdAt
     */
    private $createdAt;
    /**
     * @var Price
     */
    private $price;
    /**
     * @var Amount
     */
    private $amount;

    /**
     * Product constructor.
     * @param ProductId $productId
     * @param Name $name
     * @param SellerId $sellerId
     * @param Price $price
     * @param Amount $amount
     */
    public function __construct(
        ProductId $productId,
        Name $name,
        SellerId $sellerId,
        Price $price,
        Amount $amount
    ) {
        $this->id = $productId;
        $this->name = $name;
        $this->sellerId = $sellerId;
        $this->createdAt = new \DateTimeImmutable();
        $this->price = $price;
        $this->amount = $amount;
    }

    /**
     * @return ProductId
     */
    public function id(): ProductId
    {
        return $this->id;
    }

    /**
     * @return Name
     */
    public function getName(): Name
    {
        return $this->name;
    }

    /**
     * @return SellerId
     */
    public function sellerId(): SellerId
    {
        return $this->sellerId;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function createdAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @return Price
     */
    public function price(): Price
    {
        return $this->price;
    }

    /**
     * @return Amount
     */
    public function amount(): Amount
    {
        return $this->amount;
    }

    public function updateAmount(Amount $amount)
    {
        $this->amount = $amount;
    }
}