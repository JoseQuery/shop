<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Product\Application\Command\DeleteProduct;

use ShoppingCart\Common\Types\Application\CommandBus\Command;

/**
 * Class DeleteProductCommand
 * @package ShoppingCart\Module\Product\Application\Command\DeleteProduct
 */
class DeleteProductCommand implements Command
{
    /**
     * @var string
     */
    private $productId;

    /**
     * DeleteProductCommand constructor.
     * @param string $productId
     */
    public function __construct(string $productId)
    {
        $this->productId = $productId;
    }

    /**
     * @return string
     */
    public function productId(): string
    {
        return $this->productId;
    }
}