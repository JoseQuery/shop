<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Product\Application\Command\DeleteProduct;

use ShoppingCart\Module\Product\Domain\ProductId;
use ShoppingCart\Module\Product\Domain\ProductRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

/**
 * Class DeleteProductCommandHandler
 * @package ShoppingCart\Module\Product\Application\Command\DeleteProduct
 */
class DeleteProductCommandHandler implements MessageHandlerInterface
{
    /**
     * @var ProductRepository
     */
    private $productRepository;

    public function __construct(ProductRepository $sellerRepository)
    {
        $this->productRepository = $sellerRepository;
    }

    /**
     * @param DeleteProductCommand $command
     */
    public function __invoke(DeleteProductCommand $command): void
    {
        $product = $this->productRepository->findOrFail(new ProductId($command->productId()));

        $this->productRepository->delete($product);
    }
}