<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Product\Application\Command\AddProduct;

use ShoppingCart\Common\Types\Domain\Uuid;
use ShoppingCart\Module\Product\Domain\Amount;
use ShoppingCart\Module\Product\Domain\Name;
use ShoppingCart\Module\Product\Domain\Price;
use ShoppingCart\Module\Product\Domain\Product;
use ShoppingCart\Module\Product\Domain\ProductRepository;
use ShoppingCart\Module\Product\Domain\SellerId;
use ShoppingCart\Module\Seller\Domain\SellerRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

/**
 * Class AddProductCommandHandler
 * @package ShoppingCart\Module\Product\Application\Product\Command\AddProduct
 */
class AddProductCommandHandler implements MessageHandlerInterface
{
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var SellerRepository
     */
    private $sellerRepository;

    /**
     * AddProductCommandHandler constructor.
     * @param ProductRepository $productRepository
     * @param SellerRepository $sellerRepository
     */
    public function __construct(ProductRepository $productRepository, SellerRepository $sellerRepository)
    {
        $this->productRepository = $productRepository;
        $this->sellerRepository = $sellerRepository;
    }

    /**
     * @param AddProductCommand $command
     */
    public function __invoke(AddProductCommand $command): Uuid
    {
        $this->sellerRepository->findOrFail(new \ShoppingCart\Module\Seller\Domain\SellerId($command->sellerId()));

        $product = new Product(
            $this->productRepository->nextId(),
            new Name($command->name()),
            new SellerId($command->sellerId()),
            new Price($command->price()),
            new Amount($command->amount())
        );

        $this->productRepository->save($product);

        return $product->id();
    }
}
