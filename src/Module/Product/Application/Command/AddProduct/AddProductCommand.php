<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Product\Application\Command\AddProduct;

use ShoppingCart\Common\Types\Application\CommandBus\Command;

/**
 * Class AddProductCommand
 * @package ShoppingCart\Module\Product\Application\Product\Command\AddProduct
 */
class AddProductCommand implements Command
{
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $sellerId;
    /**
     * @var float
     */
    private $price;

    /**
     * @var int
     */
    private $amount;

    /**
     * AddProductCommand constructor.
     * @param string $name
     * @param string $sellerId
     * @param float $price
     */
    public function __construct(string $name, string $sellerId, float $price, int $amount)
    {
        $this->name = $name;
        $this->sellerId = $sellerId;
        $this->price = $price;
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    public function sellerId() : string
    {
        return $this->sellerId;
    }

    public function price() : float
    {
        return $this->price;
    }

    public function amount(): int
    {
        return $this->amount;
    }
}
