<?php

declare(strict_types=1);

namespace ShoppingCart\Module\Seller\Infrastructure\Symfony\Bundle;

use ShoppingCart\Module\Seller\Infrastructure\Symfony\DependencyInjection\SellerExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class SellerBundle extends Bundle
{
    protected function getContainerExtensionClass()
    {
        return SellerExtension::class;
    }
}
