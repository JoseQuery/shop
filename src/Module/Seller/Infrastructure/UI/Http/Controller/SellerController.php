<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Seller\Infrastructure\UI\Http\Controller;

use ShoppingCart\Module\Seller\Application\Command\DeleteSeller\DeleteSellerCommand;
use Symfony\Component\Validator\Constraints as Assert;
use ShoppingCart\Common\Types\Infrastructure\Ui\Http\Rest\Controller\Controller;
use ShoppingCart\Module\Seller\Application\Command\AddSeller\AddSellerCommand;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class SellerController extends Controller
{
    /**
     *
     * @Route("/seller", name="post_seller", methods={"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \ShoppingCart\Common\Types\Infrastructure\Ui\Http\Rest\Exception\BadFormatException
     */
    public function postSeller(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $this->validateSeller($data);
        $resourceId = $this->dispatchCommand(
            new AddSellerCommand(
                $data['seller_name']
            )
        );

        return $this->buildResponseForOk($resourceId);
    }

    /**
     *
     * @Route("/seller/{sellerId}", name="delete_seller", methods={"DELETE"})
     *
     * @param string $sellerId
     * @return JsonResponse
     */
    public function deleteSeller(string $sellerId): JsonResponse
    {
        $this->dispatchCommand(new DeleteSellerCommand($sellerId));

        return $this->buildResponseForOk();
    }

    /**
     * @param array $seller
     * @throws \ShoppingCart\Common\Types\Infrastructure\Ui\Http\Rest\Exception\BadFormatException
     */
    private function validateSeller(array $seller) : void
    {
        $contractConstraint = new Assert\Collection([
            'seller_name' => new Assert\NotBlank()
        ]);

        $this->validate($contractConstraint, $seller);
    }
}
