<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Seller\Infrastructure\Persistence\Doctrine\Type;

use ShoppingCart\Common\Types\Infrastructure\Persistence\Doctrine\Type\DoctrineId;
use ShoppingCart\Module\Seller\Domain\SellerId;

/**
 * Class SellerId
 * @package ShoppingCart\Module\Seller\Infrastructure\Persistence\Doctrine\Type
 */
class DoctrineSellerId extends DoctrineId
{
    public function className(): string
    {
        return SellerId::class;
    }
}