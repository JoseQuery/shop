<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Seller\Infrastructure\Persistence\Doctrine;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Ramsey\Uuid\Uuid;
use ShoppingCart\Module\Seller\Domain\Exception\ProductAlreadyExistsException;
use ShoppingCart\Module\Seller\Domain\Exception\SellerNotFoundException;
use ShoppingCart\Module\Seller\Domain\Seller;
use ShoppingCart\Module\Seller\Domain\SellerId;
use ShoppingCart\Module\Seller\Domain\SellerRepository;

class DoctrineSellerRepository implements SellerRepository
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Seller $seller
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(Seller $seller): void
    {
        try {
            $this->entityManager->persist($seller);
            $this->entityManager->flush($seller);
        } catch (UniqueConstraintViolationException $e) {
            throw new ProductAlreadyExistsException($seller->id());
        }
    }

    /**
     * @param Seller $seller
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function delete(Seller $seller): void
    {
        $this->entityManager->remove($seller);
        $this->entityManager->flush($seller);
    }

    /**
     * @return SellerId
     */
    public function nextId(): SellerId
    {
        return new SellerId(Uuid::uuid4()->toString());
    }

    public function findOrFail(SellerId $sellerId): Seller
    {
        $seller = $this->entityManager->getRepository(Seller::class)->find($sellerId);
        if ($seller === null) {
            throw new SellerNotFoundException($sellerId);
        }

        return $seller;
    }
}
