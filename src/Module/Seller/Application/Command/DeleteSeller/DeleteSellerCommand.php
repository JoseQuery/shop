<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Seller\Application\Command\DeleteSeller;

use ShoppingCart\Common\Types\Application\CommandBus\Command;

/**
 * Class DeleteSellerCommand
 * @package ShoppingCart\Module\Seller\Application\Command\DeleteProduct
 */
class DeleteSellerCommand implements Command
{
    /**
     * @var string
     */
    private $sellerId;

    /**
     * DeleteSellerCommand constructor.
     * @param string $sellerId
     */
    public function __construct(string $sellerId)
    {
        $this->sellerId = $sellerId;
    }

    /**
     * @return string
     */
    public function sellerId(): string
    {
        return $this->sellerId;
    }
}