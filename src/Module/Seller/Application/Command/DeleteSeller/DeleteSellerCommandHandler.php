<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Seller\Application\Command\DeleteSeller;

use ShoppingCart\Module\Seller\Domain\SellerId;
use ShoppingCart\Module\Seller\Domain\SellerRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

/**
 * Class DeleteSellerCommandHandler
 * @package ShoppingCart\Module\Seller\Application\Command\DeleteProduct
 */
class DeleteSellerCommandHandler implements MessageHandlerInterface
{
    /**
     * @var SellerRepository
     */
    private $sellerRepository;

    public function __construct(SellerRepository $sellerRepository)
    {
        $this->sellerRepository = $sellerRepository;
    }

    /**
     * @param DeleteSellerCommand $command
     */
    public function __invoke(DeleteSellerCommand $command): void
    {
        $seller = $this->sellerRepository->findOrFail(new SellerId($command->sellerId()));

        $this->sellerRepository->delete($seller);
    }
}