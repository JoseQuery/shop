<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Seller\Application\Command\AddSeller;

use ShoppingCart\Common\Types\Application\CommandBus\Command;

/**
 * Class AddSellerCommand
 * @package ShoppingCart\Module\Seller\Application\Seller\Command\AddProduct
 */
class AddSellerCommand implements Command
{
    /**
     * @var string
     */
    private $name;

    /**
     * AddSellerCommand constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }
}
