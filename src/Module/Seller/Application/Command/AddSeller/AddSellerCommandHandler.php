<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Seller\Application\Command\AddSeller;

use ShoppingCart\Common\Types\Domain\Uuid;
use ShoppingCart\Module\Seller\Domain\Name;
use ShoppingCart\Module\Seller\Domain\Seller;
use ShoppingCart\Module\Seller\Domain\SellerRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

/**
 * Class AddSellerCommandHandler
 * @package ShoppingCart\Module\Seller\Application\Seller\Command\AddProduct
 */
class AddSellerCommandHandler implements MessageHandlerInterface
{
    /**
     * @var SellerRepository
     */
    private $sellerRepository;

    public function __construct(SellerRepository $sellerRepository)
    {
        $this->sellerRepository = $sellerRepository;
    }

    /**
     * @param AddSellerCommand $command
     * @return Uuid
     */
    public function __invoke(AddSellerCommand $command): Uuid
    {
        $seller = new Seller(
            $this->sellerRepository->nextId(),
            new Name($command->name())
        );

        $this->sellerRepository->save($seller);

        return $seller->id();
    }
}