<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Seller\Domain;

/**
 * Class Seller
 *
 * @package ShoppingCart\Module\Seller\Domain
 */
class Seller
{
    /**
     * @var SellerId
     */
    private $id;
    /**
     * @var Name
     */
    private $name;

    /**
     * @var \DateTimeImmutable
     */
    private $createdAt;

    /**
     * Seller constructor.
     * @param SellerId $id
     * @param Name $name
     */
    public function __construct(SellerId $id, Name $name)
    {
        $this->id = $id;
        $this->name = $name;
        $this->createdAt = new \DateTimeImmutable();
    }

    public function id() : SellerId
    {
        return $this->id;
    }
}
