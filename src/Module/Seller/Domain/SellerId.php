<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Seller\Domain;

use ShoppingCart\Common\Types\Domain\Uuid;

/**
 * Class SellerId
 * @package ShoppingCart\Module\Seller\Domain
 */
class SellerId extends Uuid
{
}