<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Seller\Domain\Exception;


use ShoppingCart\Common\Types\Domain\Exception\DomainException;
use ShoppingCart\Module\Seller\Domain\SellerId;

class DuplicateSellerException extends DomainException
{
    /**
     * @var SellerId
     */
    private $sellerId;

    public function __construct(SellerId $sellerId)
    {
        $this->sellerId = $sellerId;

        parent::__construct();
    }

    protected function errorMessage(): string
    {
        return "Seller not found with id: {$this->sellerId->value()} already exists";
    }
}