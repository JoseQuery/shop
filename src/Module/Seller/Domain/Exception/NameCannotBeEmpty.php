<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Seller\Domain\Exception;

use ShoppingCart\Common\Types\Domain\Exception\DomainException;
use Throwable;

/**
 * Class PartyNameCannotBeEmpty
 * @package ShoppingCart\Module\Contract\Domain\Exception
 */
class NameCannotBeEmpty extends DomainException
{
    /**
     * @return string
     */
    protected function errorMessage(): string
    {
        return 'Seller name cannot be empty';
    }
}