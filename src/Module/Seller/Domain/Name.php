<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Seller\Domain;

use ShoppingCart\Common\Types\Domain\ValueObject;
use ShoppingCart\Module\Seller\Domain\Exception\NameCannotBeEmpty;

/**
 * Class PartName
 * @package ShoppingCart\Module\Contract\Domain\Parts
 */
class Name extends ValueObject
{
    /**
     * @var string
     */
    private $name;

    /**
     * Name constructor.
     *
     * @param string $name
     * @throws NameCannotBeEmpty
     */
    public function __construct(string $name)
    {
        $this->assertValue($name);
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    protected function equalValues(ValueObject $o): bool
    {
        return $this->name() === $o->value();
    }

    /**
     * @param string $name
     * @throws NameCannotBeEmpty
     */
    private function assertValue(string $name)
    {
        if (strlen($name) < 1) {
            throw new NameCannotBeEmpty();
        }
    }
}
