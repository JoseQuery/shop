<?php
declare(strict_types=1);

namespace ShoppingCart\Module\Seller\Domain;

use ShoppingCart\Module\Seller\Domain\Exception\ProductAlreadyExistsException;
use ShoppingCart\Module\Seller\Domain\Exception\SellerNotFoundException;

interface SellerRepository
{
    /**
     * @return SellerId
     */
    public function nextId(): SellerId;

    /**
     * @param Seller $lender
     *
     * @throws ProductAlreadyExistsException
     */
    public function save(Seller $lender): void;

    /**
     * @param Seller $seller
     *
     * @throws SellerNotFoundException
     */
    public function delete(Seller $seller): void;

    /**
     * @param SellerId $sellerId
     * @return Seller
     */
    public function findOrFail(SellerId $sellerId) : Seller;
}
