
<?php

return [
    Symfony\Bundle\FrameworkBundle\FrameworkBundle::class => ['all' => true],
    Doctrine\Bundle\DoctrineBundle\DoctrineBundle::class => ['all' => true],
    JMS\SerializerBundle\JMSSerializerBundle::class => ['all' => true],
    ShoppingCart\Module\Seller\Infrastructure\Symfony\Bundle\SellerBundle::class => ['all' => true],
    ShoppingCart\Module\Product\Infrastructure\Symfony\Bundle\ProductBundle::class => ['all' => true],
    ShoppingCart\Module\Cart\Infrastructure\Symfony\Bundle\CartBundle::class => ['all' => true],
    ShoppingCart\Module\Transaction\Infrastructure\Symfony\Bundle\TransactionBundle::class => ['all' => true],
    Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle::class => ['all' => true],
    Symfony\Bundle\MakerBundle\MakerBundle::class => ['dev' => true],
];
